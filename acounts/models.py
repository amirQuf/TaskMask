from django.db import models

from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    bio  = models.CharField(max_length=150,blank=True, null=True)
    profile = models.ImageField(upload_to = "user/profile")

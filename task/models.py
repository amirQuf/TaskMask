from django.db import models
from django.utils import timezone
from acounts.models import User

class Task(models.Model):
    tittle = models.CharField(max_length =300)
    created= models.DateTimeField()
    updated= models.DateTimeField()
    user  = models.ForeignKey(User , on_delete = models.CASECADE)
    description = models.TextField(null=True , Blank =True)
    cover = models.ImageField(null=True , Blank =True)
    date =  models.DateTimeField(default =timezone.now)
